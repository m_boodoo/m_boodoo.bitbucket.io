<?php
    function generateJWT() {
        return "";
    }
;?>

<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<style>
    .container-fluid {
        padding-top: 40px;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h3>Billing Form</h3>
            <div class="alert alert-info">
                This example defaults to sandbox mode (i.e. $config->setDemo(true) in the integration.php. Enter a test card no. and then move the cursor off the card input field <br />
                To test card enrolled use card no: 4111111111111112 <br />
                To test card not enrolled use card no: 4111111111111111 <br />
                You can also use this example to validate your api key and secret by switching $config->setDemo(false) in the integration.php and setting your supplied api key and secret<br />
                You use the browser's console to see debug statements when information in entered into the form (you should turn this off before going live by removing the verbose option in the JS SDK initialization
            </div>
            <form id="billing-form" action="confirmation.php" method="post">
                <input type="hidden" name="x_transaction_id" value="<?=uniqid();?>" data-threeds="id" />
                <div class="form-group">
                    <label>Amount</label>
                    <input type="text" name="x_amount" value="10" data-threeds="amount" />
                </div>
                <p>
                    <label>Card No.</label>
                    <input type="text" name="x_card_num" value="" data-threeds="pan" />
                </p>
                <p>
                    <label>Card Expiration Month</label>
                    <input type="text" name="x_exp_month" value="12" data-threeds="month" />
                </p>
                <p>
                    <label>Card Expiration Year</label>
                    <input type="text" name="x_exp_year" value="18" data-threeds="year"/>
                </p>
                <p>
                    <label>First Name</label>
                    <input type="text" name="first_name" value="Foo" />
                </p>
                <p>
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="Bar" />
                </p>
                <button>Submit</button>
            </form>
        </div>
    </div>

</div><!-- /.container -->
<script src="https://cdn.3dsintegrator.com/threeds.min.2.0-alpha.js"></script>
<script type="application/javascript">
    var tds = new ThreeDS("billing-form","<insert-your-api-key-here>","<?php echo generateJWT();?>",{
        endpoint:"https://sandbox.3dsintegrator.com"
    });
</script>
<!-- Latest compiled and minified JavaScript -->
</body>
</html>
